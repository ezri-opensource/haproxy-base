#!/bin/bash

./clean.sh
git clone https://github.com/docker-library/haproxy ci-haproxy
mv ci-haproxy/1.8/alpine/* .

rm -rf ci-haproxy

cat Dockerfile | sed 's/FROM .*/FROM ezrinimbus\/alpine:latest/' > Dockerfile2
rm Dockerfile
mv Dockerfile2 Dockerfile


if [ -z "`cat Dockerfile | grep -i ezrinimbus`" ]; then
   echo "Dockerfile substitution did not work!!"
   exit 1
fi
