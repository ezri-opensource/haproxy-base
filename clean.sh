#!/bin/bash

if [ -e Dockerfile ]; then
   rm -f Dockerfile
fi

if [ -e docker-entrypoint.sh ]; then
  rm -f docker-entrypoint.sh
fi

if [ -e ci-haproxy ]; then
   rm -rf ci-haproxy
fi
